using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSVmapperProject
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //string rawCSV = System.IO.File.ReadAllText(@"C:\Users\\kopru\source\repos\CSVmapperProject\CSVmapperProject\CSVfiles\RCH_OBG_CMPOS_REF_20210824153959.csv");
            
            //Read the contents of the csv files as individual lines 
            string[] csvLines = System.IO.File.ReadAllLines(@"C:\Users\kopru\source\repos\CSVmapperProject\CSVmapperProject\CSVfiles\RCH_OBG_CMPOS_REF_20210824153959.csv");
            
            //object where to store data
            var rch_cmpos_ref_obj = new List<RCH_cmpos_ref>();


            // Reads all lines in the csv object with a loop.
            // i starts with 1 so the header in the csv is skipped. Only data is necessary and not column names
            for (int i = 1; i < csvLines.Length; i++)
            {
                //read lines in csv file 
                RCH_cmpos_ref r = new RCH_cmpos_ref(csvLines[i]);
                rch_cmpos_ref_obj.Add(r); // stores all lines in arraylist
            }

           //Loop as many object are available and write in console
            for (int i = 0; i < rch_cmpos_ref_obj.Count; i++)
            {
                Console.WriteLine(rch_cmpos_ref_obj[i]);
            }


            Console.ReadKey();
        }

      
    }
}
