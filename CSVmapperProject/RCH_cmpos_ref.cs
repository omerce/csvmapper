﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSVmapperProject
{
    public class RCH_cmpos_ref
    {
        //variables(columns)
        public string obg_no;
        public string obg_ssm;
        public string obg_ssm_code;
        public string obg_cmpos_code;
        public string glbl_party_no;
        public string glbl_party_ssm_code;

        //Constructor
        public RCH_cmpos_ref(string rowData)
        {
            //Split each row into column data.
            string[] data = rowData.Split(',');

            //Parse data in properties.
            this.obg_no = data[0];
            this.obg_ssm_code = data[1];
            this.obg_cmpos_code = data[2];
            this.glbl_party_no = data[3];
            this.glbl_party_ssm_code = data[4];
        }

        //String format of the objects so it can be printed in console at proper way
        public override string ToString()
        {
            string str = $"{obg_no} {obg_ssm_code} {obg_cmpos_code} {glbl_party_no} {glbl_party_ssm_code}";
            return str;
        }
    }
}
